#include <mpi.h>
#include "common/utility.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

int main(int argc, char *argv[]) MPI_MAIN_TAG(MPI_ERRORS_RETURN,
{
    if(0 == mpi_rank) {
        int buf = 0xEDA;

        printf("Hello from 0\n");
        fflush(stdout);

        MPI_GUARDED_CALL(MPI_Send(&buf, 1, MPI_INT, 1, 0, MPI_COMM_WORLD));
        MPI_GUARDED_CALL(MPI_Recv(&buf, 1, MPI_INT, mpi_size - 1, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE));
    } else {
        int buf = 0;

        MPI_GUARDED_CALL(MPI_Recv(&buf, 1, MPI_INT, mpi_rank - 1, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE));

        printf("Hello from %d\n", mpi_rank);
        fflush(stdout);

        int dest = (mpi_rank + 1 == mpi_size) ? 0 : mpi_rank + 1;
        MPI_GUARDED_CALL(MPI_Send(&buf, 1, MPI_INT, dest, 0, MPI_COMM_WORLD));
    }
})

